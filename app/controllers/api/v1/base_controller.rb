module Api
  module V1
    class BaseController < Api::BaseController
      
      def busca_ponto
        if !params[:lat].nil? && !params[:lon].nil?
          @lat = params[:lat].to_f
          @lon = params[:lon].to_f
        end
        @ponto = Ponto.where(numero: params[:ponto]).first
        return render json: { status: 'PONTO_NAO_ENCONTRADO' } if @ponto.nil?
      end
      
      def busca_linha
        if !params[:lat].nil? && !params[:lon].nil?
          @lat = params[:lat].to_f
          @lon = params[:lon].to_f
        end
        @linha = Linha.includes(:pontos).where(numero: params[:linha]).first
        return render json: { status: 'LINHA_NAO_ENCONTRADO' } if @linha.nil?
        @pontos = @linha.pontos.includes(:linhas)
      end
      
      def busca_lugar
        return render json: { status: 'INVALID_REQUEST', reason: 'MISSING lat / lon' } if (params[:lat].nil? || params[:lon].nil?)
        @lat = params[:lat].to_f
        @lon = params[:lon].to_f
        @pontos = Ponto.includes(:linhas).em_torno([@lat, @lon], params[:meters])
      end
      
      def lista_linhas
        Resque.enqueue(UpdateAllJob)
        render json: { linhas: Linha.all.map { |l| { numero: l.numero, nomes: l.nome.split(', ') } } }
      end
      
      def test
        render json: {status: 'OK', version: 1}
      end
      
    end
  end
end
