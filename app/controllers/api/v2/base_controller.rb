module Api
  module V2
    class BaseController < Api::BaseController
      
      def test
        render json: {status: 'OK', version: 2}
      end
      
    end
  end
end
