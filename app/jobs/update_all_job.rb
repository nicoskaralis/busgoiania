require 'rmtc_request'

require 'update_region_job'

class UpdateAllJob
  extend RMTCRequest
  @queue = 'low'
  def self.perform
    puts 'Rodando FullUpdateJob'
    
    # # Marcando todos os pontos e linhas como não updated
    # Ponto.update_all(updated: false)
    # Linha.update_all(updated: false)
    
    @url = "http://www.rmtcgoiania.com.br"
    doc = get_doc(@url)
    
    xpath = '//*[@class="item-154" or @class="item-155" or @class="item-156" or @class="item-190"]/a'
    urls = doc.xpath(xpath).map{ |a|
      url = a.attr(:href)
      [url.split("/").last, url]
    }
    
    urls.each do |regiao, url|
      Resque.enqueue(UpdateRegionJob, regiao, url)
    end
    
    true
  end
end  
