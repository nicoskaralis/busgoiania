require 'rmtc_request'

require 'update_line_job'

class UpdateEixoListJob
  extend RMTCRequest
  @queue = 'medium'
  def self.perform(eixo)
    raise 'Parse Error' if eixo.nil?
    
    puts "Rodando UpdateEixoListJob(#{eixo})"
    @url = "http://www.rmtcgoiania.com.br/index.php?option=com_rmtclinhas&view=eixo&eixo=#{eixo}&format=raw"

    doc = get_doc(@url)
    doc.xpath('/html/body/div/ul//a').map do |a|
      Resque.enqueue(UpdateLineJob, a.content.to_i, eixo)
    end

    true
  end
end
