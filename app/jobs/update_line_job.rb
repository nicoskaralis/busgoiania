require 'rmtc_request'

class UpdateLineJob
  extend RMTCRequest
  @queue = 'low'
  def self.perform(num, seccao)
    raise 'Parse Error' if num.nil? || seccao.nil?
    numero = '%03d' % num
    
    if seccao =~ /[Ee]ixo/
      @url = "http://www.rmtcgoiania.com.br/index.php?option=com_rmtclinhas&view=linhaeixo&eixo=#{seccao}&prefixo=#{seccao}&linha=#{numero}&format=raw"
    else
      @url = "http://www.rmtcgoiania.com.br/index.php?option=com_rmtclinhas&view=linha&linha=#{numero}&tipo=#{seccao}&format=raw"
    end
    doc = get_doc(@url)
    
    full_nome = doc.xpath('/html/body/h2')[0].content.gsub!(doc.xpath('/html/body/h2/span')[0].content, '')
    nomes = full_nome.strip.split(' / ')
    
    html_freq = "http://www.rmtcgoiania.com.br/components/com_rmtclinhas/frequencia/#{numero}.html"
    
    if seccao =~ /[Ee]ixo/
      path_ida = parse_kml("http://www.rmtcgoiania.com.br/components/com_rmtclinhas/satelite/#{seccao}-#{numero}ida.kml?t=#{Time.now.to_i}")
      path_volta = parse_kml("http://www.rmtcgoiania.com.br/components/com_rmtclinhas/satelite/#{seccao}-#{numero}volta.kml?t=#{Time.now.to_i}")
    else
      path_ida = parse_kml("http://www.rmtcgoiania.com.br/components/com_rmtclinhas/satelite/#{numero}ida.kml?t=#{Time.now.to_i}")
      path_volta = parse_kml("http://www.rmtcgoiania.com.br/components/com_rmtclinhas/satelite/#{numero}volta.kml?t=#{Time.now.to_i}")
    end
    
    ln = Linha.where(numero: num).first_or_initialize
      ln.nomes = nomes
      ln.path = [path_ida, path_volta]
      ln.pontos = []
      ln.updated = true
    ln.save!
    
    @json_url = "http://www.rmtcgoiania.com.br/index.php?option=com_rmtclinhas&view=ped&format=json&linha=#{numero}"
    json = get_json(@json_url)
    raise json[:mensagem] if json[:status] == 'erro'
    raise 'Unknown Error' if json[:status] != 'sucesso'
    
    json[:pontosparada].each do |ponto|
      pt = Ponto.where(numero: ponto[:PontoId]).first_or_initialize
        pt.latitude = ponto[:PontoLatitude]
        pt.longitude = ponto[:PontoLongitude]
        pt.endereco = ponto[:PontoEndereco]
        pt.referencia = ponto[:PontoReferencia]
        pt.updated = true
      pt.save!
      ln.pontos << pt
    end
    
    ActiveRecord::Base.connection.execute("BEGIN TRANSACTION; END;")
    
    true
  end
  
private
  def self.parse_kml(url)
    xml = get_xml(url).at_xpath('//Placemark/LineString/coordinates')
    if xml.nil?
      []
    else
      xml.content.split(',0 ').map do |coord|
        coord.gsub("\n", '').gsub("\t", '').split(',').reverse
      end.reject{|a| a.empty?}
    end
  end
end