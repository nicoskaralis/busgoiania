require 'rmtc_request'

require 'update_eixo_list_job'
require 'update_line_job'

class UpdateRegionJob
  extend RMTCRequest
  @queue = 'medium'
  def self.perform(regiao, path)
    raise 'Parse Error' if regiao.nil? || path.nil?
    
    puts "Rodando UpdateRegionJob(#{regiao})"
    
    @url = "http://www.rmtcgoiania.com.br#{path}"
    doc = get_doc(@url)
    
    doc.xpath('//*[@id="accordion_menuarea"]/div[2]/ul//a').map do |a|
      if regiao =~ /citybus/
        a.attr('onclick') =~ /javascript:mostrarLinha\('([0-9]{3})', 'citybus'\);/
        Resque.enqueue(UpdateLineJob, $1.to_i, 'citybus')
      else
        a.attr('onclick') =~ /javascript:mostrarEixo\('([A-Za-z0-9\-]*)'\)/
        Resque.enqueue(UpdateEixoListJob, $1)
      end
    end
    
    return if regiao =~ /citybus/
    
    puts "Linhas que não sao do eixo" if $debug == true
    
    doc.xpath('//*[@id="accordion_menuarea"]/div[3]/ul[@class="LinhaExpressa-Botao"]//a').map do |a|
      Resque.enqueue(UpdateLineJob, a.content.to_i, 'LinhaExpressa')
    end

    doc.xpath('//*[@id="accordion_menuarea"]/div[4]/ul[@class="LinhaAlimentadora-Botao"]//a').map do |a|
      Resque.enqueue(UpdateLineJob, a.content.to_i, 'LinhaAlimentadora')
    end
    
    doc.xpath('//*[@id="accordion_menuarea"]/div[5]/ul[@class="Linhas24Horas-Botao"]//a').map do |a|
      Resque.enqueue(UpdateLineJob, a.content.to_i, '24Horas')
    end
    
    true
  end
end