class Assoc < ActiveRecord::Base
  belongs_to :ponto
  belongs_to :linha
  
  validates_presence_of :ponto
  validates_presence_of :linha
end
