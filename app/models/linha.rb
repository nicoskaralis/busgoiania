require 'parser'

class Linha < ActiveRecord::Base
  include Comparable
  
  attr_accessor :tempo
  
  has_many :assocs
  has_many :pontos, :through => :assocs
  
  validates_presence_of :numero
  validates_uniqueness_of :numero
  
  def onibus_ativos
    Parser.get_onibus(self.numero)
  end
  
  def path
    array = YAML::load(self.caminho)
    return {ida: [], volta: []} unless array
    array.map! do |a|
      a.map! do |b|
        b.map!(&:to_f)
      end
    end
    {ida: array[0][0..-2], volta: array[1][0..-2]}
  end
  
  def path=(pontos)
    self.caminho = YAML::dump(pontos)
  end
  
  def nomes=(nomes)
    self.nome = nomes.join(', ')
  end
  
  def nomes
    self.nome.split(', ')
  end
  
  # compareTo
  def <=>(anOther)
    numero <=> anOther.numero
  end
  
end
