require 'parser'

class Ponto < ActiveRecord::Base
  include Comparable
  reverse_geocoded_by :latitude, :longitude
  
  has_many :assocs
  has_many :linhas, :through => :assocs
  
  validates_presence_of :numero
  validates_uniqueness_of :numero
  
  class << self
    def em_torno(position, radius = nil)
      radius = ((radius.nil? || radius.empty?) ? 500 : radius).to_i / 1000.0
      near(position, radius, :units => :km).sort
    end
  end
  
  def linhas_com_tempo
    status, tempo = Parser.get_tempo(self.numero)
    puts tempo
    linhas = self.linhas.map do |obj|
      obj.tempo = tempo[obj.numero]
      obj
    end
    
    [status, linhas]
  end
  
  def line_numbers
    linhas.collect{|linha| linha.numero}
  end
  
  # compareTo
  def <=>(anOther)
    if self.respond_to? :distance
      self.distance<=>anOther.distance
    else
      self.numero<=>anOther.numero
    end
  end
end
