json.num @linha.numero
json.nms @linha.nomes
json.pst @linha.onibus_ativos[:status] == 'sucesso'
json.nbs @linha.onibus_ativos[:onibus] do |onibus|
  json.num onibus[:Numero]
  json.lat onibus[:Latitude]
  json.lon onibus[:Longitude]
  json.cad onibus[:Acessivel]
  json.sit ['Adiantado', 'NoHorario', 'Atrasado'].index(onibus[:Situacao])

  json.dst onibus[:Destino][:DestinoCurto]
end
json.rota do
  json.pts @pontos do |ponto|
    json.num ponto.numero
    json.end ponto.endereco
    json.ref ponto.referencia
    json.lns ponto.line_numbers
    json.dst (@lat.nil? || @lon.nil? ? nil : (ponto.distance_to([@lat, @lon]) * 1000).to_i)
    json.pos do
      json.lat ponto.latitude.to_f
      json.lon ponto.longitude.to_f
    end
  end
  json.kml @linha.path
end
