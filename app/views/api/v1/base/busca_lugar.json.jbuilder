json.pnts do
  json.array! @pontos do |ponto|
    json.num ponto.numero
    json.end ponto.endereco
    json.ref ponto.referencia
    json.lns ponto.line_numbers
    json.dst (ponto.distance * 1.609344 * 1000).to_i
    json.pos do
      json.lat ponto.latitude.to_f
      json.lon ponto.longitude.to_f
    end
  end
end
