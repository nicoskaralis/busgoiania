status, linhas = @ponto.linhas_com_tempo
dist = (@lat.nil? || @lon.nil? ? nil : (@ponto.distance_to([@lat, @lon]) * 1000).to_i)

json.status status
json.pnt_inf do
  json.num @ponto.numero
  json.end @ponto.endereco
  json.ref @ponto.referencia
  json.lnhs linhas do |linha|
    json.num linha.numero
    json.nms linha.nomes
    json.tmp linha.tempo
  end
  json.dst dist unless dist.nil?
  json.pos do
    json.lat @ponto.latitude.to_f
    json.lon @ponto.longitude.to_f
  end
end
