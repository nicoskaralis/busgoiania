# encoding: utf-8
require 'clockwork'
require File.expand_path('../config/boot', File.dirname(__FILE__))
require File.expand_path('../config/environment', File.dirname(__FILE__))

puts "Starting clock"

module Clockwork

  every(1.week, 'schedule.cleanup.db', at: '06:00') do
    # Resque.enqueue(CleanupDatabaseJob)
  end
  
  every(1.week, 'schedule.update.db', at: '17:53') do
    Resque.enqueue(UpdateAllJob)
  end
  
  # every(1.month, 'schedule.update.addr_from_points', at: '07:30') do
  #   Resque.enqueue(UpdateAddFromPointsJob)
  # end

end
