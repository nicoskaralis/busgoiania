Geocoder.configure(
  # set default units to kilometers:
  :units => :km,
  :distances => :spherical
)
