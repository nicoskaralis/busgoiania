Rails.application.routes.draw do
  
  scope module: :api, defaults: {format: :json} do
    
    scope module: :v2, constraints: ApiConstraints.new(version: 2) do
      get '/test' => 'base#test'
    end
    
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
      get '/test'      => 'base#test'
      
      get '/p/:ponto'  => 'base#busca_ponto'
      get '/l/:linha'  => 'base#busca_linha'
      get '/b/:meters' => 'base#busca_lugar'
      
      get '/ll'        => 'base#lista_linhas'
    end
  end
  
  mount Resque::Server, :at => "/resque"
  # landing page
  root to: 'pages#home'
  
end
