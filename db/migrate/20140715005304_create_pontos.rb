class CreatePontos < ActiveRecord::Migration
  def change
    create_table :pontos do |t|
      t.integer :numero, null: false
      t.decimal :latitude
      t.decimal :longitude
      t.string :endereco
      t.string :referencia
      t.boolean :updated

      t.timestamps
    end
    add_index :pontos, [:latitude, :longitude]
  end
end
