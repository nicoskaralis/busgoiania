class CreateLinhas < ActiveRecord::Migration
  def change
    create_table :linhas do |t|
      t.integer :numero, null: false
      t.string :nome
      t.string :caminho
      t.boolean :updated

      t.timestamps
    end
  end
end
