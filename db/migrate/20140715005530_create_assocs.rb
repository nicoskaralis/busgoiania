class CreateAssocs < ActiveRecord::Migration
  def change
    create_table :assocs do |t|
      t.integer :ponto_id
      t.integer :linha_id

      t.timestamps
    end
  end
end
