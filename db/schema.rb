# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140715005530) do

  create_table "assocs", force: true do |t|
    t.integer  "ponto_id"
    t.integer  "linha_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "linhas", force: true do |t|
    t.integer  "numero",     null: false
    t.string   "nome"
    t.string   "caminho"
    t.boolean  "updated"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pontos", force: true do |t|
    t.integer  "numero",     null: false
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.string   "endereco"
    t.string   "referencia"
    t.boolean  "updated"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pontos", ["latitude", "longitude"], name: "index_pontos_on_latitude_and_longitude"

end
