require 'rmtc_request'

class Parser
  extend RMTCRequest
  class << self
    def test
      puts "Funcionando"
    end
    
    def get_tempo(num_ponto)
      @url = "http://wap.rmtcgoiania.com.br/index/resultado?stopPointId=#{num_ponto}"
      doc = post_doc(@url)
      tempo = {}
      return ['NETWORK_ERROR', tempo] if doc.nil?
      return ['TEMPO_INDISPONIVEL', tempo] if doc.to_s.encode("UTF-8")[/Serviço temporariamente indisponível./]
      
      doc.xpath('//table/tbody/tr').each do |tr|
        numero = tr.at_xpath("#{tr.path}/td[1]").content
        prox = tr.at_xpath("#{tr.path}/td[3]").content.strip
        segt = tr.at_xpath("#{tr.path}/td[4]").content.strip
        tempo[numero] = {prox: prox, segt: segt}
      end
      
      ["OK", tempo]
    end
    
    def get_onibus(num_linha)
      @url = "http://www.rmtcgoiania.com.br/index.php?option=com_rmtclinhas&view=cconaweb&format=json&linha=#{num_linha}"
      get_json(@url)
    end
  end
end
