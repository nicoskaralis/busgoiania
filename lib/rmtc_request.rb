require 'net/http'
require 'nokogiri'
require 'json'

module RMTCRequest
  def get_doc(url)
    Nokogiri::HTML(get(url))
  end
  
  def post_doc(url)
    Nokogiri::HTML(post(url))
  end
  
  def get_xml(url)
    Nokogiri::XML(get(url)).remove_namespaces!
  end
  
  # def get_json(url)
  #   raw = get(url)
  #   begin
  #     JSON.parse(raw, :symbolize_names => true)
  #   rescue
  #     {}
  #   end
  # end
  
  def get_json(url)
    uri = (url.is_a?(String) ? URI(url) : url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    request.initialize_http_header({'Referer' => uri.host})
    raw = http.request(request).body
    begin
      JSON.parse(raw, :symbolize_names => true)
    rescue
      {}
    end
  end

private
  def get(url)
    uri = (url.is_a?(String) ? URI(url) : url)
    puts "HttpRequest#get #{uri}" if $debug == true
    begin
      raw = Net::HTTP.get(uri)
    rescue
      puts "HttpRequest#get Connection error" if $debug == true
      raw = nil
    ensure
      return raw
    end
  end
  
  def post(url)
    uri = URI(url)
    https = Net::HTTP.new(uri.host, uri.port)
    https.post(uri.path, uri.query, {"Referer" => uri.host}).body
  end
  
end