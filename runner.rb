#!/usr/bin/env ruby
# $:.unshift(File.expand_path("../lib", __FILE__))
# $:.unshift(File.expand_path("../app/jobs", __FILE__))
require File.expand_path('./config/boot', File.dirname(__FILE__))
require File.expand_path('./config/environment', File.dirname(__FILE__))

require 'update_all_job'
require 'parser'

$debug = false

puts "Agente rodando..."
# # Full update
# UpdateAllJob.perform()

# # Update regiao citybus
# UpdateRegionJob.perform('citybus', '/linhas-e-trajetos/citybus')

# # Update regiao sul
# UpdateRegionJob.perform('area-sul', '/linhas-e-trajetos/area-sul')

# # Update Eixo Eixo24deOutubro
# UpdateEixoListJob.perform('Eixo24deOutubro')

# # Update linha 263 do eixo EixoCampusPcdaBiblia
# UpdateLineJob.perform(263, 'EixoCampusPcdaBiblia')
# # Update da linha expressa 601
# UpdateLineJob.perform(601, 'LinhaExpressa')
# # Update da linha alimentadora 184
# UpdateLineJob.perform(184, 'LinhaAlimentadora')
# # Update da linha 24horas 951
# UpdateLineJob.perform(951, '24Horas')

Parser.test

puts Parser.get_tempo(3355)
puts Parser.get_tempo(42)
# puts Parser.get_onibus(263)